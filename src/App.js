import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import ClaimForm from './components/ClaimForm';
import ClaimList from './components/ClaimList';
import ClaimDetails from './components/ClaimDetails';

function App() {
  return (
    <Router>
      <div className="App">
        <Routes>
          <Route path="/" element={<ClaimList />} />
          <Route path="/new-claim" element={<ClaimForm />} />
          <Route path="/claims/:id" element={<ClaimDetails />} />
        </Routes>
      </div>
    </Router>
  );
}

export default App;