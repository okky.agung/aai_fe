import React, { useState } from 'react';

const ClaimForm = () => {
  const [formData, setFormData] = useState({
    patientName: '',
    dateOfService: '',
    medicalProvider: '',
    diagnosis: '',
    claimAmount: '',
    status: '',
  });
  
  const [error, setError] = useState('');

  const handleChange = (e) => {
    const { name, value } = e.target;
    if(name == "dateOfService"){
          // Convert local datetime to UTC timestamp with time zone (timestampz)
          const localDate = new Date(value);
          const utcDate = new Date(localDate.getTime() + localDate.getTimezoneOffset() * 60000); // Convert to UTC


          setFormData({
            ...formData,
            [name]: utcDate,
          });
    }else{
        setFormData({ ...formData, [name]: value });
    }
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
	
    if (!formData.status) {
      setError('Please select a status');
      return;
    }

    try {
      const response = await fetch('/api/Insurance/submit', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'accept' : 'text/plain'
        },
        body: JSON.stringify(formData)
      });

      if (!response.ok) {
        throw new Error('Network response was not ok');
      }

      const data = await response.json();
      console.log('Form data submitted successfully:', data);

      // Optionally, clear the form after successful submission
      setFormData({
        patientName: '',
        dateOfService: '',
        medicalProvider: '',
        diagnosis: '',
        claimAmount: '',
        status: '',
      });
      setError('');  // Clear any existing errors

    } catch (error) {
      console.error('Error submitting form data:', error);
      setError('Error submitting form data. Please try again later.');
    }
  };

  return (
    <div>
      <h2>Claim Form</h2>
      <form onSubmit={handleSubmit}>
        <label>
          Patient Name:
          <input type="text" name="patientName" value={formData.patientName} onChange={handleChange} />
        </label>
        <br />
        <label>
          Date of Service:
          <input type="datetime-local" name="dateOfService" value={formData.dateOfService} onChange={handleChange} />
        </label>
        <br />
        <label>
          Medical Provider:
          <input type="text" name="medicalProvider" value={formData.medicalProvider} onChange={handleChange} />
        </label>
        <br />
        <label>
          Diagnosis:
          <input type="text" name="diagnosis" value={formData.diagnosis} onChange={handleChange} />
        </label>
        <br />
        <label>
          Claim Amount:
          <input type="text" name="claimAmount" value={formData.claimAmount} onChange={handleChange} />
        </label>
        <br />
        <label>
          Status:
          <select name="status" value={formData.status} onChange={handleChange}>
            <option value="">Select Status</option>
            <option value="Pending">Pending</option>
            <option value="Approved">Approved</option>
            <option value="Rejected">Rejected</option>
          </select>
        </label>
        <br />
        <button type="submit">Submit</button>
        {error && <p style={{ color: 'red' }}>{error}</p>}
      </form>
    </div>
  );
};

export default ClaimForm;
