import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { useParams } from 'react-router-dom';

const ClaimDetails = () => {
  const { id } = useParams();  // Correct way to get route params in React Router v6
  const [claim, setClaim] = useState(null);

  useEffect(() => {
    const fetchClaim = async () => {
      try {
        const response = await axios.get(`http://localhost:5063/api/Insurance/${id}`);
        setClaim(response.data);
      } catch (error) {
        alert('Error fetching claim details!');
      }
    };

    fetchClaim();
  }, [id]);

  if (!claim) return <div>Loading...</div>;

  return (
    <div>
      <h2>Claim Details</h2>
      <p>ID: {claim.id}</p>
      <p>Status: {claim.status}</p>
      <p>Description: {claim.diagnosis}</p>
    </div>
  );
};

export default ClaimDetails;
