import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import "./ClaimList.css";

const ClaimList = () => {
  const [claims, setClaims] = useState([]);

  useEffect(() => {
    const fetchClaims = async () => {
      try {
        const response = await axios.get('http://localhost:5063/api/Insurance');
        setClaims(response.data);
      } catch (error) {
        alert('Error fetching claims!');
      }
    };

    fetchClaims();
  }, []);

  return (
    <div>
      <h2>Claim List</h2>
      <Link to={`/new-claim`}>New Data</Link>
      <table>
        <thead>
          <tr>
            <th>ID</th>
            <th>Status</th>
            <th>Diagnosis</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {claims.map((claim) => (
            <tr key={claim.id}>
              <td>{claim.id}</td>
              <td>{claim.status}</td>
              <td>{claim.diagnosis}</td>
              <td>
                <Link to={`/claims/${claim.id}`}>View Details</Link>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default ClaimList;
